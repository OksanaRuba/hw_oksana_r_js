// функція-конструктор, за допомогою якою створюємо об'єкт людина (Human з великої літери підсказує нам, що це функція-конструктор)
function Human(name,age) {
this.name = name;
this.age = age;
document.write(`Hello! My name is ${this.name} me ${this.age} <br/>`)    
}
// створюємо перемінну та задаємо масив 
const a = new Human( `Oksana`, 24);
const ageArr = [new Human(`Іvanna`, 46), new Human(`Kate`,34), a];

// для того, щоб впорядкувати числові значення в порядку зростання, або зменшення нам необхідно використовувати стрілочну функцію, яка задасть критерії сортування
function sortByAge(humans,people) {
  const array = humans.sort(function(a,b) {
  return people >= 1 ? a.age - b.age : b.age - a.age; 
})  
return array; 
}

const ageAge = sortByAge(ageArr,0);
document.write(`Зменшення <br> ${JSON.stringify(ageAge)}<br>`); //Метод JSON.stringify (ageAge) бере об'єкт і перетворює його в рядок.
const ageAge_2 = sortByAge(ageArr,1);
document.write(`Зростання <br> ${JSON.stringify(ageAge_2)}<br>`);//(JavaScript Object Notation)раніше до даного методу використовували toString


//друга задача
function humanHuman(name, age) {
// властивість экземпляра
  this.name = name;
  this.age = age;

// метод экземпляра
this.print = function () {
document.write("(" + this.name + ", " + this.age + ")<br />");
    }
}
// створення екземпляру та робота із властивостями і методами екземпляру
const p1 = new humanHuman("Dima", 46);
p1.name = "Dima";
p1.age = 27;
p1.print();

// створюємо прототип
humanHuman.prototype.printPrinte = function() {
document.write(`Hello! My name is ${this.name} me ${this.age} <br/>`);
};
const peopleP = new humanHuman('Anton', 27);
peopleP.printPrinte(); 





