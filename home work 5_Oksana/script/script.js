const documentDocument = { //створюємо об'єкт із ключами та властивостями
	header_1: "start",
    body_1:"main",
    footer_1:"end",
    data_1: "today",
                   // робимо перший вкладений об'єкт 
		application: {
			   header:{ //вкладаємо у application чотири вкладених об'єкта, які матимуть одне спільне значення wordTranslated і йому задами різні параметри
			   wordTranslated: 23,
			   }, 

			   body:{
			   wordTranslated: 73,
			   },

			   footer:{
			   wordTranslated: 93,
			   },

			   data:{
			   wordTranslated: 43,
			   },
		},

		show: function (textWrite) { //створюємо функція, яка передає один аргумент і буде відображати документ
		document.write("<p> У Вашій вкладеності "+ textWrite + " буде таке значення: " + this.application[textWrite].wordTranslated + "<br> <hr>");
		},

		replace: function (textWrite,textReplace) {//створюємо функція, яка передає два аргумента і буде замінювати нам значення на нове
	    document.write("<p> У Вашій вкладеності "+ textWrite + " буде змінено значення параметра: " + this.application[textWrite].wordTranslated + " на значення " + textReplace + "<br> <hr>" )
        this.application[textWrite].wordTranslated = textReplace;

        document.write(" зараз воно має значення: " +  this.application[textWrite].wordTranslated );
        },
};

const other = "body"; //робимо заміну нашим значенням, сюди можемо ставити, одне із чотирьох вкладених об'єктів у application:header,body,footer,data

documentDocument.show(other);

documentDocument.replace(other, 16);




